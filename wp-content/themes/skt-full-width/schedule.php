<?php 
/*
 * Template Name: Schedule*/
/**
 * The Template for displaying schedule page.
 *
 * @package SKT Full Width
 */

get_header();
?>


<ul id="schedule-bubbles" class="bubbles">

	<!-- First Course-->
	<li class="layer">
		<div class="bubble-black"></div>
	</li>

	<li class="layer">
		<div class="bubble-blue"></div>
	</li>

	<!--1-->
	<li class="layer">
		<a href="1_course"><div class="bubble-blue-second bubble-menu"></div></a>
	</li>

	<!-- 2-->
	<li class="layer">
		<a href="2_course"><div class="bubble-green bubble-menu"></div></a>
	</li>


	<li class="layer">
		<div class="bubble-orange"></div>
	</li>

	<!-- First Fourth-->
	<li class="layer">
		<a href="3_course"><div class="bubble-orange-large bubble-menu"></div></a>
	</li>

	<!-- 5-->
	<li class="layer">
		<a href="5_course"><div class="bubble-red bubble-menu"></div></a>
	</li>

	<li class="layer">
		<div class="bubble-red-second"></div>
	</li>

	<!--4-->
	<li class="layer">
		<a href="4_course"><div class="bubble-violet-large bubble-menu"></div></a>
	</li>

	<li class="layer">
		<div class="bubble-violet"></div>
	</li>

</ul>
<!--<script type="text/javascript" src="wp-content/themes/skt-full-width/js/custom.js"></script>-->


<?php wp_footer(); ?>
